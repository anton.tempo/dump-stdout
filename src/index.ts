import { spawn } from 'child_process'

async function main() {
  const child = spawn("bash", [ "-c", "yes 'long line of text' | head -n 1000" ])
  child.stdout.on('data', (data : Buffer) => {
    console.log(`out: ${data.toString()}`)
  })
  await new Promise(resolve => child.on('exit', resolve))
}

main()